import React from 'react'
import instructions from './simon.png'

function SimonSays(props) {
  return (
    <img src={instructions} alt="instructions"/>
  )
}

export default SimonSays
