import { withStyles } from '@material-ui/core'
import Radio from '@material-ui/core/Radio'
import React from 'react'

function ColoredRadioButton(props) {
  const styles = {
    root: {
      color: props.color,
      '&$checked': {
        color: props.color,
      },
    },
    checked: {},
  }
  const Comp = withStyles(styles)((props) => <Radio {...{ ...props, ...{ color: 'default' } }} />)
  return <Comp {...props} />
}

export default ColoredRadioButton

