import FormControl from '@material-ui/core/FormControl'
import Input from '@material-ui/core/Input/Input'
import InputLabel from '@material-ui/core/InputLabel/InputLabel'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import _ from 'lodash'
import React, { useState } from 'react'
import instructions from './morse.png'


const WORDS = [
  { label: 'shell', frequency: 3.505 },
  { label: 'halls', frequency: 3.515 },
  { label: 'slick', frequency: 3.522 },
  { label: 'trick', frequency: 3.532 },
  { label: 'boxes', frequency: 3.535 },
  { label: 'leaks', frequency: 3.542 },
  { label: 'strobe', frequency: 3.545 },
  { label: 'bistro', frequency: 3.552 },
  { label: 'flick', frequency: 3.555 },
  { label: 'bombs', frequency: 3.565 },
  { label: 'break', frequency: 3.572 },
  { label: 'brick', frequency: 3.575 },
  { label: 'steak', frequency: 3.582 },
  { label: 'sting', frequency: 3.592 },
  { label: 'vector', frequency: 3.595 },
  { label: 'beats', frequency: 3.600 },
]


function MorseCode(props) {
  const [letters, setLetters] = useState('')
  const lettersCount = _.countBy(_.split(letters, ''))
  const relevantWords = _.filter(WORDS, word => {
    const wordLettersCount = _.countBy(_.split(word.label, ''))
    for (const key of Object.keys(lettersCount)) {
      if (!wordLettersCount[key] || wordLettersCount[key] < lettersCount[key]) {
        return false
      }
    }
    return true
  })

  return (
    <React.Fragment>
      <div>
        <img src={instructions} alt="instructions"/>
      </div>
      <FormControl>
        <InputLabel htmlFor="component-simple">Letters (in any order)</InputLabel>
        <Input id="component-simple" value={letters} onChange={(e) => setLetters(e.target.value)}/>
      </FormControl>
      <div>
        <List dense={true}>
          {relevantWords.map((word) => (
            <ListItem key={word.frequency}>
              <ListItemText
                primary={word.label}
                secondary={word.frequency}
              />
            </ListItem>
          ))}
        </List>
      </div>
    </React.Fragment>
  )
}

export default MorseCode
