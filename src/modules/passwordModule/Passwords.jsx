import Input from '@material-ui/core/Input/Input'
import InputLabel from '@material-ui/core/InputLabel/InputLabel'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { makeStyles } from '@material-ui/core/styles'
import _ from 'lodash'
import React, { useState } from 'react'


const WORDS = [
  'about',
  'after',
  'again',
  'below',
  'could',
  'every',
  'first',
  'found',
  'great',
  'house',
  'large',
  'learn',
  'never',
  'other',
  'place',
  'plant',
  'point',
  'right',
  'small',
  'sound',
  'spell',
  'still',
  'study',
  'their',
  'there',
  'these',
  'thing',
  'think',
  'three',
  'water',
  'where',
  'which',
  'world',
  'would',
  'write',
]

const useStyles = makeStyles((theme) => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  formPart: {
    marginBottom: '2rem',
  },
  input: {
    marginTop: 0,
  },
}))

function Passwords(props) {
  const classes = useStyles()
  const [possibleLetters, setPossibleLetters] = useState(['', '', '', '', ''])
  const relevantWords = _.filter(WORDS, word => {
    for (let i = 0; i < possibleLetters.length; i++) {
      if (possibleLetters[i] && !_.includes(possibleLetters[i], word[i])) {
        return false
      }
    }
    return true
  })
  const setPossibility = (index, letters) => {
    const newPossibleLetters = _.clone(possibleLetters)
    newPossibleLetters[index] = letters
    setPossibleLetters(newPossibleLetters)
  }

  return (
    <React.Fragment>
      <div className={classes.form}>
        {_.range(possibleLetters.length).map(i => (
          <div className={classes.formPart}>
            <InputLabel htmlFor="component-simple">Letters of position {i + 1}</InputLabel>
            <Input className={classes.input} id="component-simple" value={possibleLetters[i]}
                   onChange={(e) => setPossibility(i, e.target.value)}/>
          </div>
        ))}
      </div>
      <div>
        <List dense={true}>
          {relevantWords.map((word) => (
            <ListItem>
              <ListItemText
                primary={word}
              />
            </ListItem>
          ))}
        </List>
      </div>
    </React.Fragment>
  )
}

export default Passwords
