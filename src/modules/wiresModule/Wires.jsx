import FormControl from '@material-ui/core/FormControl'
import IconButton from '@material-ui/core/IconButton'
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import DeleteIcon from '@material-ui/icons/Delete'
import _ from 'lodash'
import React, { useState } from 'react'
import { colors } from './consts'
import Wire from './Wire'

const MAX_WIRES = 6

function CalculateWireToCut(props) {
  const { wiresColors } = props
  const [lastDigitInSerial, setLastDigitInSerial] = useState('')
  const colorCount = _.countBy(wiresColors)
  const lastDigitInSerialForm = (
    <FormControl>
      <InputLabel htmlFor="component-simple">Last digit in serial</InputLabel>
      <Input id="component-simple" value={lastDigitInSerial}
             onChange={(e) => {
               const digit = e.target.value.substr(e.target.value.length - 1)
               if (_.toInteger(digit)) {
                 setLastDigitInSerial(digit)
               }
             }}
      />
    </FormControl>
  )
  switch (wiresColors.length) {
    default:
      return <span>Need 3 wires at least</span>
    case 3:
      if (!_.includes(wiresColors, colors.RED)) {
        return <span>2</span>
      }
      if (wiresColors[wiresColors.length - 1] === colors.WHITE) {
        return <span>{wiresColors.length}</span>
      }
      if (colorCount[colors.BLUE] > 1) {
        return <span>{wiresColors.lastIndexOf(colors.BLUE) + 1}</span>
      }
      return <span>{wiresColors.length}</span>
    case 4:
      if (colorCount[colors.RED] > 1) {
        if (lastDigitInSerial) {
          if (_.toInteger(lastDigitInSerial) % 2 === 0) {
            return <React.Fragment>{lastDigitInSerialForm} {wiresColors.lastIndexOf(colors.RED) + 1}</React.Fragment>
          }
        } else {
          return <React.Fragment>{lastDigitInSerialForm}</React.Fragment>
        }
      }
      if (wiresColors[wiresColors.length - 1] === colors.YELLOW && !colorCount[colors.RED]) {
        return <React.Fragment>{lastDigitInSerialForm} 1</React.Fragment>
      }
      if (colorCount[colors.BLUE] === 1) {
        return <React.Fragment>{lastDigitInSerialForm} 1</React.Fragment>
      }
      if (colorCount[colors.YELLOW] > 1) {
        return <React.Fragment>{lastDigitInSerialForm} {wiresColors.length}</React.Fragment>
      }
      return <React.Fragment>{lastDigitInSerialForm} 2</React.Fragment>
    case 5:
      if (wiresColors[wiresColors.length - 1] === colors.BLACK) {
        if (lastDigitInSerial) {
          if (_.toInteger(lastDigitInSerial) % 2 === 0) {
            return <React.Fragment>{lastDigitInSerialForm} 4</React.Fragment>
          }
        } else {
          return <React.Fragment>{lastDigitInSerialForm}</React.Fragment>
        }
      }
      if (colorCount[colors.RED] === 1 && colorCount[colors.YELLOW] > 1) {
        return <React.Fragment>{lastDigitInSerialForm} 1</React.Fragment>
      }
      if (!colorCount[colors.BLACK]) {
        return <React.Fragment>{lastDigitInSerialForm} 2</React.Fragment>
      }
      return <React.Fragment>{lastDigitInSerialForm} 1</React.Fragment>
    case 6:
      if (!colorCount[colors.YELLOW]) {
        if (lastDigitInSerial) {
          if (_.toInteger(lastDigitInSerial) % 2 === 0) {
            return <React.Fragment>{lastDigitInSerialForm} 3</React.Fragment>
          }
        } else {
          return <React.Fragment>{lastDigitInSerialForm}</React.Fragment>
        }
      }
      if (colorCount[colors.YELLOW] === 1 && colorCount[colors.WHITE] > 1) {
        return <React.Fragment>{lastDigitInSerialForm} 4</React.Fragment>
      }
      if (!colorCount[colors.RED]) {
        return <React.Fragment>{lastDigitInSerialForm} {wiresColors.length}</React.Fragment>
      }
      return <React.Fragment>{lastDigitInSerialForm} 4</React.Fragment>
  }
}

function Wires(props) {
  const [wiresColors, setWiresColors] = useState([])
  const setWireColor = (color, wireIndex) => {
    const newWires = _.clone(wiresColors)
    newWires[wireIndex] = color
    setWiresColors(newWires)
  }
  const removeLastWireColor = () => {
    setWiresColors(_.clone(_.slice(wiresColors, 0, wiresColors.length - 1)))
  }
  const wiresNumber = wiresColors.length < MAX_WIRES ? wiresColors.length + 1 : wiresColors.length
  const wires = _.range(wiresNumber).map(i => (
    <Wire
      selectedColor={wiresColors[i]}
      setSelectedColor={newColor => setWireColor(newColor, i)}
      key={i}
    />
  ))
  const wireToCut = <CalculateWireToCut {...{ wiresColors }} />

  return (
    <div>
      {wires}
      <div>
        <IconButton aria-label='delete' onClick={removeLastWireColor}>
          <DeleteIcon/>
        </IconButton>
        {wireToCut || 'Unknown'}
      </div>
    </div>
  )
}

export default Wires
