import React from 'react'
import ColoredRadioButton from '../ColoredRadioButton'
import { colors } from './consts'

function Wire(props) {
  const { selectedColor, setSelectedColor } = props
  const handleChange = (event) => {
    setSelectedColor(event.target.value)
  }

  return (
    <div>
      {Object.keys(colors).map((color) => (
        <ColoredRadioButton
          checked={selectedColor === colors[color]}
          onChange={handleChange}
          value={colors[color]}
          name="wire-radio-button"
          inputProps={{ 'aria-label': colors[color] }}
          color={colors[color]}
          key={color}
        />
      ))}
    </div>
  )
}

export default Wire
