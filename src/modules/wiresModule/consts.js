const colors = {
  RED: 'red',
  WHITE: 'white',
  BLUE: 'blue',
  YELLOW: 'yellow',
  BLACK: 'black',
}

export {
  colors,
}
