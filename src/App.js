import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import { makeStyles } from '@material-ui/core/styles'
import React from 'react'
import './App.css'
import MorseCode from './modules/morseModule/MorseCode'
import Passwords from './modules/passwordModule/Passwords'
import SimonSays from './modules/simonModule/SimonSays'
import Wires from './modules/wiresModule/Wires'

const useStyles = makeStyles((theme) => ({
  expansionPanelDetails: {
    backgroundColor: '#666',
  },
}))

function App() {
  const classes = useStyles()

  return (
    <div>
      <ExpansionPanel>
        <ExpansionPanelSummary>
          Wires
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.expansionPanelDetails}>
          <Wires/>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel>
        <ExpansionPanelSummary>
          Passwords
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Passwords/>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel>
        <ExpansionPanelSummary>
          Morse Code
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <MorseCode/>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel>
        <ExpansionPanelSummary>
          Simon Says
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <SimonSays/>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  )
}

export default App
